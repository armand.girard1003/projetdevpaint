package com.projectdev;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class GestionSouris implements MouseListener, MouseMotionListener{
	private ArdoiseMagique ardoise;				// Reference to the ArdoiseMagique instance
	private int x1, y1, x2, y2;					// Coordinates for drawing lines
	
	public GestionSouris(ArdoiseMagique ardoise) {
		this.ardoise = ardoise;
        this.ardoise.addMouseListener(this);	this.ardoise.addMouseMotionListener(this);	// Add this GestionSouris instance as a mouse listener to the ArdoiseMagique instance   
	}
	
	/*----------MouseMotionListener----------*/
	@Override
	public void mouseDragged(MouseEvent e) {
        Graphics gc = ardoise.getGraphics();	// Get the graphics context of the ArdoiseMagique
        x2 = e.getX();	y2 = e.getY();			// Get the current mouse position
        gc.drawLine(x1, y1, x2, y2);			// Draw a line from the previous mouse position to the current mouse position
        x1 = x2;	y1 = y2;					// Update the starting coordinates for the next line segment
	}

	@Override
	public void mouseMoved(MouseEvent e) {}
	/*---------------------------------------*/
	
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	/*-------------MouseListener-------------*/
	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		x1 = e.getX();	y1 = e.getY();			// Get the starting coordinates of the line segment
	}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}
	/*---------------------------------------*/
}
