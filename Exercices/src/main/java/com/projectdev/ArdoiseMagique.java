package com.projectdev;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class ArdoiseMagique extends JFrame {
	private static final long serialVersionUID = 1L;
	
	ArdoiseMagique() {
	    JPanel panel = new JPanel();								// Create a new JPanel
	    panel.setBackground(Color.WHITE);							// Sets panel background color to white
	    getContentPane().add(panel, BorderLayout.CENTER); 			// Adds the panel to the center of the window
	    GestionSouris gestionSouris = new GestionSouris(this);		// Creates an instance of MouseManager by passing it a reference to the current instance of MagicSlate
	    panel.addMouseListener(gestionSouris);						// Adds a mouse listener for click events to this panel
	    panel.addMouseMotionListener(gestionSouris);				// Adds a mouse listener for motion events to this panel
	    setTitle("Ardoise Magique");								// Defines the window title
	    setSize(500, 500);											// Sets window size to 500x500 pixels
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);				// Defines the default operation when the window is closed
	    setLocationRelativeTo(null);								// Places the window in the center of the screen
	    setVisible(true);											// Makes the window visible
	}

}
