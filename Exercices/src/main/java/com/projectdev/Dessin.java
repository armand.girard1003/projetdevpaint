package com.projectdev;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Dessin extends JFrame{
	private static final long serialVersionUID = 1L;
	// Variable for the drawing surface
	private static JPanel monPanneau;
	
	// Constructor
	public Dessin(String titre) {
		super(titre);
		
		// Declaration of event listener
		ActionListener monEcouteur = new ActionListener() {
			// Action at an event
			public void actionPerformed(ActionEvent event) {
				System.out.println(event.getActionCommand() + " cliqué");
			}
		};
		
		
		JMenuBar barreDeMenu = new JMenuBar(); 					// Declaration of the menu bar
		setJMenuBar(barreDeMenu);								// Install barreDeMenu as a menu bar
		
		JMenu monMenu1 = new JMenu("Menu1"); 					// Declaration of the menu 1
		barreDeMenu.add(monMenu1);								// Add monMenu1 to barreDeMenu
		JMenuItem item11 = new JMenuItem("Item 1"); 			// Declaration of item 1 of menu 1
		monMenu1.add(item11); 									// Add item 11 to menu 1
		item11.addActionListener(monEcouteur); 					// Association of the listener with item 11
		JMenuItem item12 = new JMenuItem("Item 2"); 			// Declaration of item 2 of menu 1
		monMenu1.add(item12); 									// Add item 12 to menu 1
		item12.addActionListener(monEcouteur); 					// Association of the listener with item 12
		
		JMenu monMenu2 = new JMenu("Menu1"); 					// Declaration of the menu 2
		barreDeMenu.add(monMenu2);								// Add monMenu2 to barreDeMenu
		JMenuItem item21 = new JMenuItem("Item 1"); 			// Declaration of item 1 of menu 2
		monMenu2.add(item21); 									// Add item 21 to menu 2
		item21.addActionListener(monEcouteur); 					// Association of the listener with item 21
		JMenuItem item22 = new JMenuItem("Item 2"); 			// Declaration of item 2 of menu 2
		monMenu2.add(item22); 									// Add item 22 to menu 2
		item22.addActionListener(monEcouteur); 					// Association of the listener with item 22
		
		monPanneau = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				super.paintComponent(g); 						// Call the superclass method to ensure proper painting

				g.drawLine(getWidth(), 0, 0, getHeight()); 		// Draw a line from top-right to bottom-left
				g.drawLine(0, 0, getWidth(), getHeight()); 		// Draw a line from top-left to bottom-right
			}
		};
		
		monPanneau.setLayout(new GridLayout());  				// Set the layout manager for monPanneau to GridLayout
		monPanneau.setBackground(Color.white);   				// Set the background color of monPanneau to white
		getContentPane().add(monPanneau, BorderLayout.CENTER); 	// Add monPanneau to the content pane of the frame, positioned at the center
		
		setVisible(true);   									// Make the frame visible
		setSize(400, 400);  									// Set the size of the frame
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  		// Close operation when the window is closed
	}
}
