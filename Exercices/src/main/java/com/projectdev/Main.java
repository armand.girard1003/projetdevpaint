package com.projectdev;

public class Main {
    public static void main(String[] args) {
    	Question question = new Question("Ceci est le titre", "Et voila la question!");
    	Dessin dessin = new Dessin("Voila le titre");
        ArdoiseMagique ardoiseMagique = new ArdoiseMagique();
        GestionSouris gestionSouris = new GestionSouris(ardoiseMagique);
    }
}
