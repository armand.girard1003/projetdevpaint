package com.projectdev;

import java.awt.BorderLayout;
import javax.swing.*;

public class Question {
	public Question (String titre, String question) {
		JFrame f = new JFrame(titre);									// Create a new JFrame with the specified title
		JLabel txt = new JLabel(question);								// Create a JLabel with the question text
		JButton yes = new JButton("Yes");								// Create JButtons for 'yes' option
		JButton no = new JButton("No");									// Create JButtons for 'no' option
		f.getContentPane().add(txt, BorderLayout.NORTH);				// Add the JLabel to the north side of the JFrame's content pane
		f.getContentPane().add(yes, BorderLayout.WEST);					// Add the 'yes' button to the west side of the JFrame's content pane
		f.getContentPane().add(no, BorderLayout.EAST);					// Add the 'no' button to the east side of the JFrame's content pane
		f.pack();														// Pack the components in the JFrame to fit their preferred sizes
		f.setVisible(true);												// Set the JFrame to be visible
		f.setSize(500, 500);											// Set the size of the JFrame to 500x500 pixels
		f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);	// Set the default close operation to dispose of the JFrame when closed
	}
}
