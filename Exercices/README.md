# Exercices ProjetDev

Ce dossier contient le projet Eclipse "ExercicesProjetDev" qui comprend plusieurs exercices pratiques.

## Structure du Projet

- **ArdoiseMagique.java** : Une classe représentant une application Java simulant une ardoise magique.
- **PremiereFenetre.java** : Une classe pour créer une fenêtre avec un projet Java.
- **PremiersDessins.java** : Une classe pour dessiner une croix dans une fenêtre créée en Java.
- **GestionEvenements.java** : Une classe pour gérer les événements dans une application Java en liant un bouton à une action.
- **Dessin.java** : Une classe pour dessiner des formes simples dans une fenêtre graphique.
- **GestionSouris.java** : Une classe pour gérer les événements de la souris pour l'application Ardoise Magique.
- **Main.java** : La classe principale de l'application.

## Utilisation

Pour utiliser ces exercices, vous pouvez suivre ces étapes :
1. Ouvrez le projet dans Eclipse.
2. Compilez et exécutez les fichiers Java correspondants à chaque exercice.
3. Suivez les instructions fournies dans les commentaires du code pour compléter chaque exercice.

## Auteur

Ce fichier a été écrit par [Armand Girard].
