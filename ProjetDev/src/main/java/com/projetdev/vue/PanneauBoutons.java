package com.projetdev.vue;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.ImageIcon;

import com.projetdev.controleur.*;

/**
 * La classe PAnneauBoutons est le classe contenant le panneau de boutons d'interaction de l'application.
 * Elle hérite de JPanel.
 */
public class PanneauBoutons extends JPanel {
	/**
	 * Le numéro de série utilisé pour la sérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Référence vers la fenêtre principale de l'application.
	 */
	private Fenetre maFenetre;

	/**
	 * Contrôleur des boutons utilisé dans l'interface utilisateur.
	 */
	private GestionBoutons monControleur;

	/**
	 * Police de caractères utilisée pour les boutons.
	 */
	private Font font = new Font("Arial", Font.BOLD, 14);

	/**
	 * Contrôleur de couleur utilisé dans l'interface utilisateur.
	 */
	private GestionCouleur controleurCouleur;

	/**
	 * Checkbox utilisée pour sélectionner le mode de dessin (plein ou non).
	 */
	private Checkbox pleinCheckbox;

	/**
	 * Contrôleur du menu utilisé dans l'interface utilisateur.
	 */
	private GestionMenu controleurMenu;

	/**
	 * Largeur par défaut du trait de dessin.
	 */
	private int largeur = 1;
	
	/**
	 * Attribut pour stocker le chemin du symbole de Carre
	 * */
	private String carreLogoPath;
	
	/**
	 * Attribut pour stocker le chemin du symbole de Rectangle
	 * */
	private String rectangleLogoPath;
	
	/**
	 * Attribut pour stocker le chemin du symbole de Ellipse
	 * */
	private String ellipseLogoPath;
	
	/**
	 * Attribut pour stocker le chemin du symbole de Cercle
	 * */
	private String cercleLogoPath;
	
	/**
	 * Attribut pour stocker le chemin du symbole de Ligne
	 * */
	private String ligneLogoPath;
	
	/**
	 * Attribut pour stocker le chemin du symbole de Libre
	 * */
	private String libreLogoPath;
	
	/**
	 * Attribut pour stocker le chemin du symbole de Gomme
	 * */
	private String gommeLogoPath;

    /**
     * Constructeur du panneau de boutons.
     * @param maFenetre La fenêtre principale.
     */
    public PanneauBoutons(Fenetre maFenetre) {
        this.maFenetre = maFenetre;
        this.setBackground(Color.GRAY); // Définir la couleur de fond du panneau
        this.controleurCouleur = new GestionCouleur(Color.BLACK, this.maFenetre); // Initialiser le contrôleur de couleur
        this.controleurMenu = new GestionMenu(this); // Initialiser le contrôleur du menu
        
        this.monControleur = new GestionBoutons(); // Initialiser le contrôleur des boutons
        this.monControleur.setPanneauBoutons(this); // Définir ce panneau comme panneau associé au contrôleur
        this.setAlignmentX(CENTER_ALIGNMENT);
        this.setLayout(new GridLayout(5, 4)); // Définir la disposition du panneau en une grille de 5 lignes et 4 colonnes

        JButton resetButton = new JButton("Effacer"); // Bouton pour effacer le dessin
        this.pleinCheckbox = new Checkbox("Forme pleine"); // Checkbox pour sélectionner le mode de dessin (plein ou non)
        JButton choixCouleurButton = new JButton("Choisir une couleur"); // Bouton pour choisir une couleur
        
        // Déclaration du groupe de Boutons
        ButtonGroup group = new ButtonGroup();
        
        // Déclaration des boutons du groupe de boutons
        JRadioButton libreCheckBox = new JRadioButton("Libre");
        JRadioButton rectangleCheckBox = new JRadioButton("Rectangle");
        JRadioButton cercleCheckBox = new JRadioButton("Cercle");
        JRadioButton carreCheckBox = new JRadioButton("Carré");
        JRadioButton ellipseCheckBox = new JRadioButton("Ellipse");
        JRadioButton ligneCheckBox = new JRadioButton("Ligne");
        JRadioButton gommeCheckBox = new JRadioButton("Gomme");
        
        // Curseur pour la largeur de trait
        JSlider largeurSlider = new JSlider(JSlider.HORIZONTAL, 1, 50, 1); 
        JLabel labelSlider = new JLabel("Epaissseur");
        labelSlider.setHorizontalAlignment(SwingConstants.LEFT);
        
        // Ajout des boutons dans le groupe de boutons
        group.add(libreCheckBox);
        group.add(rectangleCheckBox);
        group.add(cercleCheckBox);
        group.add(carreCheckBox);
        group.add(ellipseCheckBox);
        group.add(ligneCheckBox);
        group.add(gommeCheckBox);
        
        if(this.maFenetre.isRunningFromJar()) {
        	this.carreLogoPath = "/resources/img/carre.png";
        	this.cercleLogoPath = "/resources/img/cercle.png";
        	this.rectangleLogoPath = "/resources/img/rectangle.png";
        	this.ellipseLogoPath = "/resources/img/ellipse.png";
        	this.libreLogoPath = "/resources/img/libre.png";
        	this.ligneLogoPath = "/resources/img/ligne.png";
        	this.gommeLogoPath = "/resources/img/gomme.png";
        }
        else {
        	this.carreLogoPath = "/img/carre.png";
        	this.cercleLogoPath = "/img/cercle.png";
        	this.rectangleLogoPath = "/img/rectangle.png";
        	this.ellipseLogoPath = "/img/ellipse.png";
        	this.libreLogoPath = "/img/libre.png";
        	this.ligneLogoPath = "/img/ligne.png";
        	this.gommeLogoPath = "/img/gomme.png";
        }

        ImageIcon libreIco = new ImageIcon(this.getClass().getResource(this.libreLogoPath));
        ImageIcon rectangleIco = new ImageIcon(this.getClass().getResource(this.rectangleLogoPath));
        ImageIcon cercleIco = new ImageIcon(this.getClass().getResource(this.cercleLogoPath));
        ImageIcon carreIco = new ImageIcon(this.getClass().getResource(this.carreLogoPath));
        ImageIcon ellipseIco = new ImageIcon(this.getClass().getResource(this.ellipseLogoPath));
        ImageIcon ligneIco = new ImageIcon(this.getClass().getResource(this.ligneLogoPath));
        ImageIcon gommeIco = new ImageIcon(this.getClass().getResource(this.gommeLogoPath));
        
        Image scaledLibreIco = libreIco.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT);
        Image scaledRectangleIco = rectangleIco.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT);
        Image scaledCercleIco = cercleIco.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT);
        Image scaledCarreIco = carreIco.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT);
        Image scaledEllipseIco = ellipseIco.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT);
        Image scaledLigneIco = ligneIco.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT);
        Image scaledGommeIco = gommeIco.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT);
        
        libreIco = new ImageIcon(scaledLibreIco);
        rectangleIco = new ImageIcon(scaledRectangleIco);
        cercleIco = new ImageIcon(scaledCercleIco);
        carreIco = new ImageIcon(scaledCarreIco);
        ellipseIco = new ImageIcon(scaledEllipseIco);
        ligneIco = new ImageIcon(scaledLigneIco);
        gommeIco = new ImageIcon(scaledGommeIco);
        
        libreCheckBox.setIcon(libreIco);
        rectangleCheckBox.setIcon(rectangleIco);
        cercleCheckBox.setIcon(cercleIco);
        carreCheckBox.setIcon(carreIco);
        ellipseCheckBox.setIcon(ellipseIco);
        ligneCheckBox.setIcon(ligneIco);
        gommeCheckBox.setIcon(gommeIco);
        
        // Etablissement des états par défaut (par défaut : false)
        libreCheckBox.setSelected(true);
        pleinCheckbox.setState(true);

        // Ajout des boutons au panneau et donc à la fenêtre
        this.add(libreCheckBox);
        this.add(rectangleCheckBox);
        this.add(cercleCheckBox);
        this.add(carreCheckBox);
        this.add(ellipseCheckBox);
        this.add(ligneCheckBox);
        this.add(gommeCheckBox);
        this.add(resetButton);
        this.add(choixCouleurButton);
        this.add(pleinCheckbox);
        this.add(largeurSlider);
        this.add(labelSlider);

        // Ajout des écouteurs d'événements sur les boutons
        libreCheckBox.addActionListener(monControleur);
        rectangleCheckBox.addActionListener(monControleur);
        cercleCheckBox.addActionListener(monControleur);
        carreCheckBox.addActionListener(monControleur);
        ellipseCheckBox.addActionListener(monControleur);
        ligneCheckBox.addActionListener(monControleur);
        resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getFenetre().getPanneauDessin().effacerTableauDessin();
			}
        });
        choixCouleurButton.addActionListener(controleurCouleur);
        pleinCheckbox.addItemListener(monControleur);
        largeurSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				largeur = largeurSlider.getValue();
			}
        });
        gommeCheckBox.addActionListener(monControleur);
        
        // Couleur de police pour tous les composants
        resetButton.setForeground(Color.BLACK);
        pleinCheckbox.setForeground(Color.BLACK);
        choixCouleurButton.setForeground(Color.BLACK);
        libreCheckBox.setForeground(Color.BLACK);
        rectangleCheckBox.setForeground(Color.BLACK);
        cercleCheckBox.setForeground(Color.BLACK);
        carreCheckBox.setForeground(Color.BLACK);
        ellipseCheckBox.setForeground(Color.BLACK);
        ligneCheckBox.setForeground(Color.BLACK);
        largeurSlider.setForeground(Color.BLACK);
        labelSlider.setForeground(Color.BLACK);
        gommeCheckBox.setForeground(Color.BLACK);
        
        // Couleur de fond pour les composants
        libreCheckBox.setBackground(this.getBackground());
        rectangleCheckBox.setBackground(this.getBackground());
        cercleCheckBox.setBackground(this.getBackground());
        carreCheckBox.setBackground(this.getBackground());
        ellipseCheckBox.setBackground(this.getBackground());
        ligneCheckBox.setBackground(this.getBackground());
        largeurSlider.setBackground(this.getBackground());
        labelSlider.setBackground(this.getBackground());
        gommeCheckBox.setBackground(this.getBackground());

        // Style d'écriture pour tous les composants
        resetButton.setFont(this.font);
        pleinCheckbox.setFont(this.font);
        choixCouleurButton.setFont(this.font);
        libreCheckBox.setFont(this.font);
        rectangleCheckBox.setFont(this.font);
        cercleCheckBox.setFont(this.font);
        carreCheckBox.setFont(this.font);
        ellipseCheckBox.setFont(this.font);
        ligneCheckBox.setFont(this.font);
        labelSlider.setFont(this.font);
        gommeCheckBox.setFont(this.font);
        
        // Barre de Menu déroulant
    	JMenuBar barreDeMenu = new JMenuBar(); 	
    	this.maFenetre.setJMenuBar(barreDeMenu);
        // Menu "Fichier" déroulant
        JMenu formesMenu = new JMenu("Fichier"); 				
		barreDeMenu.add(formesMenu);
		// Item "Exporter" dans le menu déroulant "Fichier"
		JMenuItem rectangleBut = new JMenuItem("Exporter en PNG"); 		
		formesMenu.add(rectangleBut);
		rectangleBut.addActionListener(controleurMenu);
		
		// Menu "Outils" déroulant
		JMenu outilsMenu = new JMenu("Outils");
		barreDeMenu.add(outilsMenu);
		// Item "Annuler" dans le menu déroulant "Outils"
		JMenuItem annulerBut = new JMenuItem("Annuler");
		outilsMenu.add(annulerBut);
		annulerBut.addActionListener(controleurMenu);
		// Item "Rétablir" dans le menu déroulant "Outils"
		JMenuItem retablirBut = new JMenuItem("Rétablir");
		outilsMenu.add(retablirBut);
		retablirBut.addActionListener(controleurMenu);
    }

    /**
     * Méthode pour obtenir la fenêtre principale.
     * @return La fenêtre principale.
     */
    public Fenetre getFenetre() {
        return this.maFenetre;
    }
    
    /**
     * Méthode pour obtenir la largeur du trait de dessin.
     * @return La largeur du trait de dessin.
     */
    public int getLargeur() {
    	return this.largeur;
    }
    
    /**
     * Méthode pour obtenir l'état de la checkbox de dessin plein.
     * @return True si le mode de dessin plein est activé, sinon false.
     */
    public boolean getStatePlein() {
    	return  this.pleinCheckbox.getState();
    }
    
    /**
     * Méthode pour obtenir le contrôleur de couleur.
     * @return Le contrôleur de couleur.
     */
    public GestionCouleur getControleurCouleur() {
    	return this.controleurCouleur;
    }
    
    /**
     * Obtient l'épaisseur actuelle.
     * @return Epaisseur actuelle.
     */
    public int getEpaisseurActuelle() {
    	return this.largeur;
    }
}
