# Package com.projetdev.vue

Ce package contient les classes qui représentent la vue de l'application.

## Contenu

1. [Fenetre.java](Fenetre.java): La classe qui gère la fenêtre principale de l'application.
2. [PanneauBoutons.java](PanneauBoutons.java): La classe qui contient le panneau de boutons d'interaction de l'application.
3. [PanneauDessin.java](PanneauDessin.java): La classe qui représente le panneau de dessin pour afficher des formes graphiques.

Chaque classe offre des fonctionnalités spécifiques pour l'interface utilisateur de l'application.
