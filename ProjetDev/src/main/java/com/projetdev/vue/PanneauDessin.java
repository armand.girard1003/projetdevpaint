package com.projetdev.vue;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import com.projetdev.controleur.GestionSouris;
import com.projetdev.modele.Forme;

/**
 * La classe PanneauDessin est la classe qui représente le panneau de dessin pour afficher des formes graphiques.
 * Elle hérite de JPanel
 */
public class PanneauDessin extends JPanel {
    
	/**
	 * Référence vers la fenêtre principale de l'application.
	 */
	private Fenetre maFenetre;

	/**
	 * La dimension du panneau de dessin.
	 */
	private Dimension dimension;
	
	/**
	 * Le numéro de série utilisé pour la sérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * La couleur de fond du panneau de dessin.
	 */
	private Color couleurDeFond;

	/**
	 * Liste des formes dessinées sur le panneau de dessin.
	 */
	private List<Forme> listeFormes = null;
	
	/**
	 * Liste de sauvegarde des formes pour annuler les modifications.
	 */
	private List<Forme> listeFormesBackUp = null;
	
	/**
	 * Liste des formes en prévisualisation.
	 */
	private List<Forme> listePrevisualisation = null;

	/**
	 * Nouvelle forme à dessiner sur le panneau.
	 */
	private Forme nouvelleForme;

	/**
	 * Contrôleur de la souris utilisé pour interagir avec le panneau de dessin.
	 */
	private GestionSouris controleurSouris;
	
	/**
	 * Opacité du logo.
	 */
	private float opacity = 1.0f;
	
	/**
	 * Liste des dessins Libres.
	 */
	private ArrayList<ArrayList<Point>> dessins = null;
	
	/**
	 * Liste des Couleurs utilisées lors des desins Libres.
	 */
	private ArrayList<Color> couleurs = null;
	
	/**
	 * Liste des épaisseurs utilisées lors des dessins Libres.
	 */
	private ArrayList<BasicStroke> epaisseurs = null;
	
	/**
	 * Liste des points du dessin Libre en cours.
	 */
	private ArrayList<Point> dessinActuel = null;
	
	/**
	 * Epaisseur actuelle.
	 */
	private int epaisseurActuelle;
	
	/**
	 * Couleur actuelle.
	 */
	private Color couleurActuelle;
	
	/**
	 * Liste de forme permettant la restauration des formes annulées.
	 */
	private List<Forme> restauration = null;
    
    /**
     * Constructeur de la classe PanneauDessin.
     * @param maFenetre La fenêtre parente du panneau.
     * @param couleurDeFond La couleur de fond du panneau.
     */
    public PanneauDessin(Fenetre maFenetre, Color couleurDeFond) {
        this.maFenetre = maFenetre;
        this.couleurDeFond = couleurDeFond;
        this.listeFormes = new ArrayList<>();
        this.listeFormesBackUp = new ArrayList<>();
        this.restauration = new ArrayList<>();
        this.dessins = new ArrayList<>();
        this.couleurs = new ArrayList<>();
        this.dessinActuel = new ArrayList<>();
        this.epaisseurs = new ArrayList<>();
        this.listePrevisualisation = new ArrayList<>();
        this.setBackground(new Color(245, 245, 220));
        this.setPreferredSize(this.dimension);
    }
    
    /**
     * Obtient le gestionnaire de souris associé au panneau.
     * @return Le gestionnaire de souris.
     */
    public GestionSouris getControleurSouris() {
        return this.controleurSouris;
    }
    
    /**
     * Définit le gestionnaire de souris associé au panneau.
     * @param controleur Le gestionnaire de souris à définir.
     */
    public void setControleurSouris(GestionSouris controleur) {
        this.controleurSouris = controleur;
    }

    /**
     * Obtient la liste des formes dessinées sur le panneau.
     * @return La liste des formes.
     */
    public List<Forme> getListeForme() {
        return this.listeFormes;
    }
    
    /**
     * Obtient la liste de sauvegarde des formes dessinées sur le panneau.
     * @return La liste de sauvegarde des formes.
     */
    public List<Forme> getListeFormeBackUp() {
        return this.listeFormesBackUp;
    }
    
    /**
     * Obtient la liste des formes en prévisualisation.
     * @return La liste des formes en prévisualisation.
     */
    public List<Forme> getListePrevisualisation(){
    	return this.listePrevisualisation;
    }
    
    /**
     * Obtient la dernière forme ajoutée au panneau.
     * @return La dernière forme ajoutée.
     */
    public Forme getNouvelleForme() {
        return this.nouvelleForme;
    }
    
    /**
	 * Définit la couleur actuelle.
	 * @param color Couleur actuelle.
	 */
    public void setCouleurActuelle(Color color) {
    	this.couleurActuelle = color;
    }
    
    /**
     * Obtient la couleur actuelle
     * @return Couleur actuelle.
     */
    public Color getCouleurActuelle() {
    	return this.couleurActuelle;
    }
    
    /**
	 * Définit l'épaisseur actuelle.
	 * @param stroke Epaisseur actuelle.
	 */
    public void setEpaisseurActuelle(int stroke) {
    	this.epaisseurActuelle = stroke;
    }
    
    /**
	 * Ajout du dessin Libre en cours à la liste des dessins Libres.
	 */
    public void addDrawing() {
    	this.dessins.add(new ArrayList<>(this.dessinActuel));
    	this.couleurs.add(this.couleurActuelle);
    	this.epaisseurs.add(new BasicStroke(this.epaisseurActuelle, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
    	this.dessinActuel.clear();
    }
    
    /**
	 * Ajout d'un point dans la liste de points du dessin Libre en cours.
	 * @param point Point ajouté à la liste de points du dessin Libre en cours.
	 */
    public void addPoint(Point point) {
    	this.dessinActuel.add(point);
    	this.repaint();
    }

    /**
     * Ajoute une nouvelle forme au panneau.
     * @param nouvelleForme La nouvelle forme à ajouter.
     */
    public void ajouterForme(Forme nouvelleForme) {
        this.nouvelleForme = nouvelleForme;
        this.listeFormes.add(this.nouvelleForme);
        this.listeFormesBackUp.add(this.nouvelleForme);
    }
    
    /**
     * Ajoute une forme en prévisualisation.
     * @param nouvelleForme Forme en prévisualisation.
     */
    public void ajouterPrevisu(Forme nouvelleForme) {
    	if(this.listePrevisualisation.size() != 0) {
    		this.listePrevisualisation.removeLast();
    	}
    	this.listePrevisualisation.add(nouvelleForme);
    }
    
    /**
     * Vider la liste de forme en prévisualisation.
     */
    public void viderPrevisu() {
    	this.listePrevisualisation.clear();;
    }
    
    /**
     * Annule la dernière forme ajoutée au panneau.
     */
    public void annulerForme() {
    	if (this.listeFormes != null) {
    		for(int i = 0 ; i < 2 ; i++) {
        		this.restauration.add(this.listeFormes.getLast());
                this.listeFormes.removeLast();	
        	}
            this.repaint();
    	}
    }
    
    /**
     * Rétablit la dernière forme annulée sur le panneau.
     */
    public void retablirForme() {
    	if (this.listeFormes != null && this.restauration != null) {
    		for(int i = 0 ; i < 2 ; i++) {
        		this.listeFormes.add(this.restauration.getLast());
                this.restauration.removeLast();
        	}
    		this.repaint();
    	}
    }
    
    /**
     * Obtient la fenêtre parente du panneau.
     * @return La fenêtre parente.
     */
    public Fenetre getFenetre() {
        return this.maFenetre;
    }
    
    /**
     * Efface toutes les formes dessinées sur le panneau.
     */
    public void effacerTableauDessin() {
        this.listeFormes.clear();
        this.dessinActuel.clear();
        this.dessins.clear();
        this.couleurs.clear();
        this.epaisseurs.clear();
        this.repaint();
    }

    /**
     * Redessine le panneau en dessinant toutes les formes.
     * @param g L'objet Graphics utilisé pour dessiner.
     */
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        
        Graphics2D g2d = (Graphics2D) g.create();
        /*
        float alpha = 0.2f;
        AlphaComposite alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
        g2d.setComposite(alphaComposite);
        
        int x = (getWidth() - logoImage.getIconWidth()) / 2;
        int y = (getHeight() - logoImage.getIconHeight()) / 2;
        
        g2d.drawImage(this.logoImage.getImage(), x, y, null);
        */
        AlphaComposite composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, this.opacity);
        g2d.setComposite(composite);
        for (Forme f : this.listeFormes) {
        	if(f != null)
                f.dessiner(g);
        }
        
        for (Forme f : this.listePrevisualisation) {
        	if(f != null)
                f.dessiner(g);
        }
        g.dispose();
    }
}
