package com.projetdev.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * La classe Fenetre est la classe qui gère la fenêtre principale de l'application.
 * Elle hérite de JFrame
 */
public class Fenetre extends JFrame {
	/**
	 * Identifiant de version de la classe utilisé lors de la sérialisation.
	 * Cet identifiant est utilisé pour garantir que la version sérialisée de la classe
	 * correspond à la version de la classe actuelle lors de la désérialisation.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Référence vers le panneau de dessin
	 * */
	private PanneauDessin monPanneauDessin;
	
	/**
	 * Référence vers le panneau de boutons
	 * */
    private PanneauBoutons monPanneauBoutons;
    
    /**
     * Instance pour contenir l'image du logo
     * */
    private ImageIcon logo = null;
    
    /**
     * Attribut pour stocker le chemin du logo
     * */
    private String logoPath;

    /**
     * Constructeur de la fenêtre.
     * @param nom Le nom de la fenêtre.
     * @param dimension La dimension de la fenêtre.
     */
    public Fenetre(String nom, Dimension dimension) {
        super(nom);
        
        if(isRunningFromJar())
        	this.logoPath = "/resources/img/logo.png";
        else
        	this.logoPath = "/img/logo.png";
        
        this.logo = new ImageIcon(this.getClass().getResource(this.logoPath));
        this.setLayout(new BorderLayout()); // Utiliser BorderLayout pour organiser les composants
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Fermer l'application lorsque la fenêtre est fermée
        this.setPreferredSize(dimension); // Définir la dimension de la fenêtre
        this.setIconImage(this.logo.getImage()); // Définir le logo de la fenêtre
        this.setResizable(true); // Déterminer si l'utilisateur peut redimensionner manuellement la fenêtre, ici NON
        this.setUndecorated(false); // Enlever la barre de titre avec les boutons de fermeture, plein écran et fenêtré
    }
    
    /**
     * Vérifie si le programme s'exécute à partir d'un fichier JAR.
     * Cette méthode utilise le nom de la classe Fenetre pour déterminer si elle est exécutée à partir d'un fichier JAR. 
     * @return true si le programme s'exécute à partir d'un fichier JAR, sinon false.
     */
    public boolean isRunningFromJar() {
        // Récupère le nom de la classe Fenetre et le convertit en format compatible avec les chemins de fichier
        String className = Fenetre.class.getName().replace('.', '/');
        // Récupère le chemin de la classe Fenetre en tant que ressource
        String classJar = Fenetre.class.getResource("/" + className + ".class").toString();
        // Vérifie si le chemin de la classe commence par "jar:"
        return classJar.startsWith("jar:");
    }
    
    /**
     * Méthode pour définir le panneau de dessin de la fenêtre.
     * @param panneau Le panneau de dessin à définir.
     */
    public void setPanneauDessin(PanneauDessin panneau) {
    	this.monPanneauDessin = panneau;
    	this.add(this.monPanneauDessin, BorderLayout.CENTER); // Ajouter le panneau de dessin au centre de la fenêtre
    }
    
    /**
     * Méthode pour définir le panneau de boutons de la fenêtre.
     * @param panneau Le panneau de boutons à définir.
     */
    public void setPanneauBoutons(PanneauBoutons panneau) {
    	this.monPanneauBoutons = panneau;
    	this.add(this.monPanneauBoutons, BorderLayout.NORTH); // Ajouter le panneau de boutons au nord de la fenêtre
    }

    /**
     * Méthode pour obtenir le panneau de dessin de la fenêtre.
     * @return Le panneau de dessin de la fenêtre.
     */
    public PanneauDessin getPanneauDessin() {
        return monPanneauDessin;
    }

    /**
     * Méthode pour obtenir le panneau de boutons de la fenêtre.
     * @return Le panneau de boutons de la fenêtre.
     */
    public PanneauBoutons getPanneauBoutons() {
        return monPanneauBoutons;
    }
}
