package com.projetdev.modele;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 * La classe Ligne représente une ligne graphique.
 * Elle hérite de Forme.
 */
public class Ligne extends Forme {
	/**
	 * Le point de fin utilisé dans les opérations de dessin.
	 */
	private Point fin;

	/**
	 * La couleur utilisée dans les opérations de dessin.
	 */
	private Color couleur;

	/**
	 * L'épaisseur du trait utilisée dans les opérations de dessin.
	 */
	private int epaisseur;

    
    /**
     * Constructeur de la classe Ligne.
     * @param origine Le point d'origine de la ligne.
     * @param fin Le point de fin de la ligne.
     * @param couleur La couleur de la ligne.
     * @param epaisseur L'épaisseur de la ligne.
     */
    public Ligne(Point origine, Point fin, Color couleur, int epaisseur) {
        super(origine, 0, 0);
        this.fin = fin;
        this.couleur = couleur;
        this.epaisseur = epaisseur;
    }

    /**
     * Méthode pour dessiner la ligne.
     * @param graphique Le contexte graphique utilisé pour dessiner.
     */
    @Override
    public void dessiner(Graphics graphique) {
        Graphics2D g2d = (Graphics2D) graphique;
        g2d.setStroke(new BasicStroke(this.epaisseur, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        graphique.setColor(this.couleur);
        graphique.drawLine((int)(origine.getX()), (int)(origine.getY()), fin.x, fin.y);
    }
}
