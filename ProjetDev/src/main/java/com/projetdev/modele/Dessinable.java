package com.projetdev.modele;

import java.awt.Graphics;

/**
 * L'interface Dessinable définit une méthode pour dessiner un objet graphique.
 */
public interface Dessinable {
    /**
     * Dessine l'objet graphique en utilisant le contexte graphique spécifié.
     * @param graphique Le contexte graphique utilisé pour dessiner l'objet.
     */
    void dessiner(Graphics graphique);
}
