package com.projetdev.modele;

import java.awt.Color;
import java.awt.Point;

/**
 * La classe Cercle représente un cercle graphique.
 * Elle hérite de Ellipse (le cercle est une ellipse particulière).
 */
public class Cercle extends Ellipse {
    /**
     * Constructeur de la classe Cercle.
     * @param origine Le point d'origine du cercle.
     * @param diametre Le diamètre du cercle.
     * @param plein Indique si le cercle est plein ou non.
     * @param couleur La couleur du cercle.
     * @param epaisseur L'épaisseur du contour du cercle.
     */
    public Cercle(Point origine, int diametre, boolean plein, Color couleur, int epaisseur) {
        super(origine, diametre, diametre, plein, couleur, epaisseur);
    }
}
