package com.projetdev.modele;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 * La classe Ellipse représente une ellipse graphique.
 * Elle hérite de Forme.
 */
public class Ellipse extends Forme {
	/**
	 * Indique si la forme doit être dessinée en mode plein ou non.
	 */
	private boolean plein;

	/**
	 * La couleur utilisée pour dessiner la forme.
	 */
	private Color couleur;

	/**
	 * L'épaisseur du trait utilisé pour dessiner la forme.
	 */
	private int epaisseur;
    
    /**
     * Constructeur de la classe Ellipse.
     * @param origine Le point d'origine de l'ellipse.
     * @param largeur La largeur de l'ellipse.
     * @param hauteur La hauteur de l'ellipse.
     * @param plein Indique si l'ellipse est pleine ou non.
     * @param couleur La couleur de l'ellipse.
     * @param epaisseur L'épaisseur du contour de l'ellipse.
     */
    public Ellipse(Point origine, int largeur, int hauteur, boolean plein, Color couleur, int epaisseur) {
        super(origine, largeur, hauteur);
        this.plein = plein;
        this.couleur = couleur;
        this.epaisseur = epaisseur;
    }

    /**
     * Méthode pour dessiner l'ellipse.
     * @param graphique Le contexte graphique utilisé pour dessiner.
     */
    @Override
    public void dessiner(Graphics graphique) {
        graphique.setColor(this.couleur);
        if(plein) {
            graphique.fillOval((int)(origine.getX()), (int)(origine.getY()), largeur, hauteur);
        } else {
            Graphics2D g2d = (Graphics2D) graphique;
            g2d.setStroke(new BasicStroke(this.epaisseur));
            graphique.drawOval((int)(origine.getX()), (int)(origine.getY()), largeur, hauteur);
        }
    }
}
