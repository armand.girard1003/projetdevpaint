package com.projetdev.modele;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

import com.projetdev.vue.PanneauDessin;

/**
 * Classe représentant un dessin libre. Elle permet de créer un dessin en traçant des lignes
 * avec des couleurs et épaisseurs variées sur un panneau de dessin.
 */
public class DessinLibre extends Forme {
    /** 
     * Ensemble de tous les dessins réalisés, chaque dessin étant une liste de points. 
     */
    private ArrayList<ArrayList<Point>> dessins;
    
    /** 
     * Liste des points constituant le dessin actuellement en cours de création. 
     */
    private ArrayList<Point> dessinActuel;
    
    /** 
     * Liste des couleurs utilisées pour chaque dessin. 
     */
    private ArrayList<Color> couleurs;
    
    /** 
     * Liste des épaisseurs de trait utilisées pour chaque dessin. 
     */
    private ArrayList<BasicStroke> epaisseurs;
    
    /** 
     * Panneau de dessin sur lequel les dessins sont réalisés. 
     */
    private PanneauDessin panneauDessin;
    
    /**
     * Mode gomme activé ou non.
     * */
    private boolean gomme = false;
    
	/**
	 * Couleur choisie pour le dessin.
	 */
    private Color couleurChoisie;
    
    /**
     * Constructeur par défaut de DessinLibre. Initialise les listes pour les dessins,
     * les couleurs, les épaisseurs de traits et le dessin actuel.
     * @param couleur La couleur du dessin.
     */
    public DessinLibre(Color couleur) {
        this.dessins = new ArrayList<>();
        this.epaisseurs = new ArrayList<>();
        this.couleurs = new ArrayList<>();
        this.dessinActuel = new ArrayList<>();
        this.couleurChoisie = couleur;
        this.gomme = true;
    }
    
	/**
	 * Constructeur par défaut de DessinLibre. Initialise les listes pour les
	 * dessins, les couleurs, les épaisseurs de traits et le dessin actuel.
	 * 
	 */
    public DessinLibre() {
        this.dessins = new ArrayList<>();
        this.epaisseurs = new ArrayList<>();
        this.couleurs = new ArrayList<>();
        this.dessinActuel = new ArrayList<>();
    }
    
    /**
     * Ajoute le dessin actuel à la liste des dessins terminés, avec sa couleur
     * et son épaisseur de trait, puis réinitialise le dessin actuel pour en commencer un nouveau.
     */
    public void ajouterDessin() {
        if(this.gomme) {
        	this.dessins.add(new ArrayList<>(this.dessinActuel));
            this.couleurs.add(this.couleurChoisie);
            this.epaisseurs.add(new BasicStroke(
                    this.panneauDessin.getFenetre().getPanneauBoutons().getEpaisseurActuelle(), 
                    BasicStroke.CAP_ROUND, 
                    BasicStroke.JOIN_ROUND));
            this.dessinActuel.clear();
        }
        else {
        	this.dessins.add(new ArrayList<>(this.dessinActuel));
            this.couleurs.add(this.panneauDessin.getCouleurActuelle());
            this.epaisseurs.add(new BasicStroke(
                    this.panneauDessin.getFenetre().getPanneauBoutons().getEpaisseurActuelle(), 
                    BasicStroke.CAP_ROUND, 
                    BasicStroke.JOIN_ROUND));
            this.dessinActuel.clear();
        }
    }
    
    /**
     * Ajoute un point au dessin actuellement en cours et met à jour le panneau de dessin.
     *
     * @param point Le point à ajouter au dessin actuel.
     * @param panneauDessin Le panneau de dessin sur lequel le point est dessiné.
     */
    public void addPoint(Point point, PanneauDessin panneauDessin) {
        this.dessinActuel.add(point);
        this.panneauDessin = panneauDessin;
        this.panneauDessin.repaint();
    }
    
    /**
     * Dessine tous les dessins stockés sur le graphique fourni.
     *
     * @param graphique Le contexte graphique sur lequel les dessins sont réalisés.
     */
    @Override
    public void dessiner(Graphics graphique) {
        Graphics2D g2d = (Graphics2D) graphique.create();
        
        AlphaComposite composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f);
        g2d.setComposite(composite);
        
        for(int i = 0; i < this.dessins.size(); i++) {
            ArrayList<Point> dessin = this.dessins.get(i);
            Color couleur = this.couleurs.get(i);
            BasicStroke epaisseur = this.epaisseurs.get(i);
            g2d.setStroke(epaisseur);
            g2d.setColor(couleur);
            for(int j = 0; j < dessin.size() - 1; j++) {
                Point p1 = dessin.get(j);
                Point p2 = dessin.get(j + 1);
                g2d.drawLine(p1.x, p1.y, p2.x, p2.y);
            }
        }
    }
}
