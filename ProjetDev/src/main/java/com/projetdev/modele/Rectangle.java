package com.projetdev.modele;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 * La classe Rectangle représente un rectangle graphique.
 * Elle hérite de Forme.
 */
public class Rectangle extends Forme {
	/**
	 * Indique si la forme est pleine ou non.
	 */
	private boolean plein;

	/**
	 * La couleur utilisée pour dessiner la forme.
	 */
	private Color couleur;

	/**
	 * L'épaisseur du trait utilisé pour dessiner la forme.
	 */
	private int epaisseur = 1;
    
    /**
     * Constructeur de la classe Rectangle.
     * @param origine Le point d'origine du rectangle.
     * @param largeur La largeur du rectangle.
     * @param hauteur La hauteur du rectangle.
     * @param plein Indique si le rectangle est plein ou non.
     * @param couleur La couleur du rectangle.
     * @param epaisseur L'épaisseur du contour du rectangle.
     */
    public Rectangle(Point origine, int largeur, int hauteur, boolean plein, Color couleur, int epaisseur) {
        super(origine, largeur, hauteur);
        this.plein = plein;
        this.couleur = couleur;
        this.epaisseur = epaisseur;
    }

    /**
     * Méthode pour dessiner le rectangle.
     * @param graphique Le contexte graphique utilisé pour dessiner.
     */
    @Override
    public void dessiner(Graphics graphique) {
        graphique.setColor(this.couleur);
        if (plein) {
            graphique.fillRect((int)(origine.getX()), (int)(origine.getY()), largeur, hauteur);
        } else {
            Graphics2D g2d = (Graphics2D) graphique;
            g2d.setStroke(new BasicStroke(this.epaisseur));
            graphique.drawRect((int)(origine.getX()), (int)(origine.getY()), largeur, hauteur);
        }
    }
}
