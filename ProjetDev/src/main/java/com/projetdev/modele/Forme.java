package com.projetdev.modele;

import java.awt.Point;

/**
 * La classe abstraite Forme représente une forme géométrique.
 * Elle implémente Dessinable.
 */
public abstract class Forme implements Dessinable{
	/**
	 * La largeur de la forme.
	 */
	protected int largeur;

	/**
	 * La hauteur de la forme.
	 */
	protected int hauteur;

	/**
	 * L'origine de la forme.
	 */
	protected Point origine;
    
    /**
     * Constructeur de la classe Forme.
     * @param origine Le point d'origine de la forme.
     * @param largeur La largeur de la forme.
     * @param hauteur La hauteur de la forme.
     */
    public Forme(Point origine, int largeur, int hauteur) {
        this.origine = origine;
        this.largeur = largeur;
        this.hauteur = hauteur;
    }
    
    /**
     * Constructeur par défaut de la classe Forme.
     * Ce constructeur est vide.
     */
    public Forme() {}
}
