# Package com.projetdev.modele

Ce package contient les classes qui représentent les modèles de données pour l'application.

## Contenu

1. [Dessinable.java](Dessinable.java): Une interface pour les objets graphiques dessinables.
2. [Forme.java](Forme.java): Une classe abstraite représentant une forme géométrique.
3. [Rectangle.java](Rectangle.java): Une classe représentant un rectangle graphique.
4. [Ellipse.java](Ellipse.java): Une classe représentant une ellipse graphique.
5. [Carre.java](Carre.java): Une classe représentant un carré graphique.
6. [Cercle.java](Cercle.java): Une classe représentant un cercle graphique.
7. [Ligne.java](Ligne.java): Une classe représentant une ligne graphique.

Chaque classe contient des méthodes spécifiques pour dessiner et manipuler les formes géométriques dans l'application.
