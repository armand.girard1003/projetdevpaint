package com.projetdev.modele;

import java.awt.Color;
import java.awt.Point;

/**
 * La classe Carre représente un carré graphique.
 * Elle hérite de Rectangle (le carré est un rectangle particulier).
 */
public class Carre extends Rectangle {
    /**
     * Constructeur de la classe Carre.
     * @param origine Le point d'origine du carré.
     * @param cote La longueur du côté du carré.
     * @param plein Indique si le carré est plein ou non.
     * @param couleur La couleur du carré.
     * @param epaisseur L'épaisseur du contour du carré.
     */
    public Carre(Point origine, int cote, boolean plein, Color couleur, int epaisseur) {
        super(origine, cote, cote, plein, couleur, epaisseur);
    }
}
