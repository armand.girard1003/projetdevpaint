package com.projetdev.controleur;

import java.awt.CheckboxGroup;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JRadioButton;
import com.projetdev.vue.*;

/**
 * La classe GestionBoutons est une classe qui gère les boutons de contrôle pour les formes graphiques.
 * Elle hérite de CheckboxGroup et implémente ItemListener et ActionListener.
 */
public class GestionBoutons extends CheckboxGroup implements ItemListener, ActionListener {
    
	/**
	 * Identifiant de version de la classe utilisé lors de la sérialisation.
	 * Cet identifiant est utilisé pour garantir que la version sérialisée de la classe
	 * correspond à la version de la classe actuelle lors de la désérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Le panneau de boutons associé à cette fenêtre.
	 */
	private PanneauBoutons panneauBoutons;

    
    /**
     * Constructeur de la classe GestionBoutons.
     */
    public GestionBoutons() {
        super();
    }

    /**
     * Définit le panneau de boutons associé à cette instance de gestion des boutons.
     * @param panneauBoutons Le panneau de boutons à définir.
     */
    public void setPanneauBoutons(PanneauBoutons panneauBoutons) {
        this.panneauBoutons = panneauBoutons;
    }

    /**
     * Méthode invoquée lorsque l'état d'un élément d'interface utilisateur qui implémente ItemListener est modifié.
     * @param e L'événement d'état de l'élément.
     */
    @Override
    public void itemStateChanged(ItemEvent e) {}

    /**
     * Méthode invoquée lorsqu'un bouton est actionné.
     * @param e L'événement d'action.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source instanceof JRadioButton) {
            JRadioButton button = (JRadioButton) source;
            this.panneauBoutons.getFenetre().getPanneauDessin().getControleurSouris().setFormeChoisie(button.getText());
        }
    }
}
