package com.projetdev.controleur;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;

import com.projetdev.vue.*;

/**
 * La classe GestionMenu est une classe qui gère les actions liées aux éléments de menu.
 * Elle implémente ActionListener.
 */
public class GestionMenu implements ActionListener{
	/**
	 * Le panneau de boutons associé à cette fenêtre.
	 */
	private PanneauBoutons panneauBoutons;
    
    /**
     * Constructeur de la classe GestionMenu.
     * @param panneauBoutons Le panneau de boutons associé à cette instance de gestion de menu.
     */
    public GestionMenu(PanneauBoutons panneauBoutons ) {
        this.panneauBoutons = panneauBoutons ;
    }
    
    /**
     * Méthode invoquée lorsqu'un élément de menu est sélectionné.
     * @param e L'événement d'action.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source instanceof JMenuItem) {
            JMenuItem boutonSelectionne = (JMenuItem) source;
            String textBouton = boutonSelectionne.getText();
            if(textBouton.equals("Exporter en PNG")) {
                JFileChooser emplacementFichier = new JFileChooser();
                int choixUtilisateur = emplacementFichier.showSaveDialog(this.panneauBoutons.getFenetre());
                if(choixUtilisateur == JFileChooser.APPROVE_OPTION) {
                    File fichierSortie = emplacementFichier.getSelectedFile();
                    String cheminFichier = fichierSortie.getAbsolutePath();
                    if(!cheminFichier.toLowerCase().endsWith(".png")) {
                        fichierSortie = new File(cheminFichier + ".png");
                    }
                    try {
                        PanneauDessin panneauDessin = this.panneauBoutons.getFenetre().getPanneauDessin();
                        BufferedImage image = new BufferedImage(panneauDessin.getWidth(), panneauDessin.getHeight(), BufferedImage.TYPE_INT_RGB);
                        Graphics2D g2 = image.createGraphics();
                        panneauDessin.paint(g2);
                        g2.dispose();
                        ImageIO.write(image, "png", fichierSortie);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            else if(textBouton.equals("Annuler")) {
                if(this.panneauBoutons.getFenetre().getPanneauDessin().getNouvelleForme() != null) {
                    this.panneauBoutons.getFenetre().getPanneauDessin().annulerForme();
                }
            }
            else if(textBouton.equals("Rétablir")) {
                if(this.panneauBoutons.getFenetre().getPanneauDessin().getListeForme() != this.panneauBoutons.getFenetre().getPanneauDessin().getListeFormeBackUp()) {
                    this.panneauBoutons.getFenetre().getPanneauDessin().retablirForme();
                }
            }
        }
    }
}
