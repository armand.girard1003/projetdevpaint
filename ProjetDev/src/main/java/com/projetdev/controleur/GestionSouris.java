package com.projetdev.controleur;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.projetdev.vue.PanneauDessin;
import com.projetdev.modele.*;

/**
 * La classe GestionSourisest une classe qui gère les interactions de la souris pour le dessin des formes.
 * Elle implémente MouseListener et MouseMotionListener.
 */
public class GestionSouris implements MouseListener, MouseMotionListener {	
	/**
	 * Référence vers le panneau de dessin utilisé pour dessiner les formes.
	 */
	private PanneauDessin monPanneau;

	/**
	 * Coordonnée x1 du point de début pour dessiner des formes.
	 */
	private int x1;

	/**
	 * Coordonnée y1 du point de début pour dessiner des formes.
	 */
	private int y1;

	/**
	 * Coordonnée x2 du point de fin pour dessiner des formes.
	 */
	private int x2;

	/**
	 * Coordonnée y2 du point de fin pour dessiner des formes.
	 */
	private int y2;

	/**
	 * Point de départ pour dessiner une ligne.
	 */
	private Point pointDepart;

	/**
	 * Point de fin pour dessiner une ligne.
	 */
	private Point pointFin;

	/**
	 * Forme sélectionnée pour le dessin.
	 * Par défaut, la forme choisie est "Libre".
	 */
	private String formeChoisie = "Libre";
	
	/**
	 * Etat du clique gauche de la souris.
	 */
	private boolean isLeftMouseButtonPressed = false;
	
	/**
	 * Instance de dessin Libre.
	 */
	private DessinLibre dessin = null;
	
	/**
	 * Dessin Libre en cours ou non.
	 */
	private boolean etatDessin = false;
    
    /**
     * Méthode pour définir la forme choisie pour le dessin.
     * @param formeChoisie La forme choisie pour le dessin.
     */
    public void setFormeChoisie(String formeChoisie) {
    	this.formeChoisie = formeChoisie;
    }

    /**
     * Constructeur par défaut de la gestion de la souris.
     */
    public GestionSouris() {}

    /**
     * Méthode pour définir le panneau de dessin associé à cette gestion de souris.
     * @param panneau Le panneau de dessin à associer.
     */
    public void setPanneauDessin(PanneauDessin panneau) {
        this.monPanneau = panneau;
        this.monPanneau.addMouseListener(this); // Ajout des écouteurs de souris au panneau
        this.monPanneau.addMouseMotionListener(this);
        this.monPanneau.setControleurSouris(this); // Définition de cette instance comme contrôleur pour le panneau
    }

    /**
     * Dessine une forme élastique pour suivre le mouvement de la souris.
     * Cette méthode est utilisée pour afficher une forme en mouvement avant qu'elle ne soit réellement dessinée.
     * @param x1 La coordonnée x du premier point.
     * @param y1 La coordonnée y du premier point.
     * @param x2 La coordonnée x du deuxième point.
     * @param y2 La coordonnée y du deuxième point.
     */
    private void dessinerElastique(int x1, int y1, int x2, int y2) {
        int xSup, ySup, xInf, yInf;
        
        // Déterminer les coordonnées supérieures et inférieures pour dessiner un rectangle élastique
        if (x1 > x2) {
            xSup = x1; 
            xInf = x2;
        } else {
            xSup = x2; 
            xInf = x1;
        }
        
        if (y1 > y2) {
            ySup = y1; 
            yInf = y2;
        } else {
            ySup = y2; 
            yInf = y1;
        }
        
        Graphics gc = this.monPanneau.getGraphics();
        try {
            gc.setXORMode(monPanneau.getBackground()); // Utiliser le mode XOR avec la couleur de fond
            gc.drawRect(xInf, yInf, xSup - xInf, ySup - yInf); // Dessiner le rectangle élastique
        } finally {
            gc.dispose(); // Libérer les ressources du contexte graphique pour éviter les fuites de mémoire
        }
    }

    /**
     * Gère l'événement de clic de souris.
     * Cette méthode est vide car elle n'a pas de fonctionnalité spécifique pour cet événement.
     * @param e L'événement MouseEvent associé au clic de souris.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        // Cette méthode est vide car elle n'a pas de fonctionnalité spécifique pour cet événement.
    }

    /**
     * Méthode appelée lorsque la souris est pressée.
     * Elle enregistre les coordonnées du point de départ pour différentes formes.
     * @param e L'événement de la souris associé à cette action.
     */
    @Override
    public void mousePressed(MouseEvent e) {
        // Récupère le bouton de forme sélectionné
        String boutonSelectionne = this.formeChoisie;
        
        // Si le bouton sélectionné est "Ligne", enregistre le point de départ pour dessiner une ligne
        if(boutonSelectionne.equals("Ligne")) {
        	this.pointDepart = e.getPoint();
        }
        // Si le bouton sélectionné est "Libre" et que le bouton gauche de la souris est pressé,
        // enregistre le point pour dessiner une forme libre
        else if (boutonSelectionne.equals("Libre")) {
            if(e.getButton() == MouseEvent.BUTTON1) {
                this.isLeftMouseButtonPressed = true;
                this.dessin = new DessinLibre();
                this.dessin.addPoint(e.getPoint(), this.monPanneau);
                this.etatDessin = true;
            }
        }
        else if (boutonSelectionne.equals("Gomme")) {
            if(e.getButton() == MouseEvent.BUTTON1) {
                this.isLeftMouseButtonPressed = true;
                this.dessin = new DessinLibre(this.monPanneau.getFenetre().getPanneauDessin().getBackground());
                this.dessin.addPoint(e.getPoint(), this.monPanneau);
                this.etatDessin = true;
            }
        }
        // Si aucun des cas précédents n'est vrai, enregistre les coordonnées du point de départ pour d'autres formes
        else {
        	this.x1 = e.getX();
        	this.y1 = e.getY();
        }
    }

    /**
     * Gère l'événement de relâchement du bouton de la souris.
     * Cette méthode est appelée lorsque le bouton de la souris est relâché après avoir été enfoncé.
     * Elle crée une nouvelle forme en fonction des coordonnées de départ et d'arrivée de la souris,
     * puis l'ajoute au panneau de dessin et demande au panneau de se redessiner pour afficher la nouvelle forme.
     * @param e L'événement MouseEvent associé au relâchement du bouton de la souris.
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        // Récupérer le bouton sélectionné à partir du contrôleur associé au panneau boutons
        String boutonSelectionne = this.formeChoisie;
        boolean statePlein = this.monPanneau.getFenetre().getPanneauBoutons().getStatePlein();
        
        // Effacer le dernier élastique dessiné
        this.dessinerElastique(this.x1, this.y1, this.x2, this.y2);
        
        // Créer la forme correspondante à la sélection
        Forme nouvelleForme = null;
        int xInf = Math.min(this.x1, this.x2);
        int yInf = Math.min(this.y1, this.y2);
        int largeur = Math.abs(this.x2 - this.x1);
        int hauteur = Math.abs(this.y2 - this.y1);
        
        // RECTANGLE
        if (boutonSelectionne.equals("Rectangle")) {
            nouvelleForme = new Rectangle(new Point(xInf, yInf), largeur, hauteur, statePlein, 
                    this.monPanneau.getFenetre().getPanneauBoutons().getControleurCouleur().getCouleurChoisie(),
                    this.monPanneau.getFenetre().getPanneauBoutons().getLargeur());
        } 
        // CARRE
        else if (boutonSelectionne.equals("Carré")) {
            int cote = Math.max(largeur, hauteur);
            nouvelleForme = new Carre(new Point(xInf, yInf), cote, statePlein, 
            		this.monPanneau.getFenetre().getPanneauBoutons().getControleurCouleur().getCouleurChoisie(),
                    this.monPanneau.getFenetre().getPanneauBoutons().getLargeur());
        } 
        // CERCLE
        else if (boutonSelectionne.equals("Cercle")) {
            int cote = Math.max(largeur, hauteur);
            nouvelleForme = new Cercle(new Point(xInf, yInf), cote, statePlein, 
            		this.monPanneau.getFenetre().getPanneauBoutons().getControleurCouleur().getCouleurChoisie(),
                    this.monPanneau.getFenetre().getPanneauBoutons().getLargeur());
        } 
        // ELLIPSE
        else if (boutonSelectionne.equals("Ellipse")) {
            nouvelleForme = new Ellipse(new Point(xInf, yInf), largeur, hauteur, statePlein, 
            		this.monPanneau.getFenetre().getPanneauBoutons().getControleurCouleur().getCouleurChoisie(),
                    this.monPanneau.getFenetre().getPanneauBoutons().getLargeur());
        } 
        // LIGNE
        else if (boutonSelectionne.equals("Ligne")) {
            this.pointFin = e.getPoint();
            nouvelleForme = new Ligne(this.pointDepart, this.pointFin, 
            		this.monPanneau.getFenetre().getPanneauBoutons().getControleurCouleur().getCouleurChoisie(),
                    this.monPanneau.getFenetre().getPanneauBoutons().getLargeur());
        }
        // LIBRE
        else if (boutonSelectionne.equals("Libre")) {
        	if(e.getButton() == MouseEvent.BUTTON1) {
        		this.isLeftMouseButtonPressed = false;
        		if(this.dessin != null)
        			this.dessin.ajouterDessin();
        	}
        }
        // GOMME
        else if (boutonSelectionne.equals("Gomme")) {
        	if(e.getButton() == MouseEvent.BUTTON1) {
        		this.isLeftMouseButtonPressed = false;
        		if(this.dessin != null)
        			this.dessin.ajouterDessin();
        	}
        }

        // Vérifier si une nouvelle forme a été créée
        if (nouvelleForme != null || this.etatDessin == true) {
            if (nouvelleForme != null) {
            	// Ajouter la forme au panneau dessin
            	this.monPanneau.ajouterForme(nouvelleForme);
            }
            else if (this.etatDessin == true) {
            	this.monPanneau.ajouterForme(this.dessin);
            	this.dessin = null;
            }
            
            this.monPanneau.viderPrevisu();
            // Demander au panneau de dessin de se redessiner pour afficher la nouvelle forme
            this.monPanneau.repaint();
        }
    }

    /**
     * Gère l'événement lorsque la souris entre dans la zone du composant.
     * Cette méthode est vide car elle n'a pas de fonctionnalité spécifique pour cet événement.
     * @param e L'événement MouseEvent associé à l'entrée de la souris.
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        // Cette méthode est vide car elle n'a pas de fonctionnalité spécifique pour cet événement.
    }

    /**
     * Gère l'événement lorsque la souris quitte la zone du composant.
     * Cette méthode est vide car elle n'a pas de fonctionnalité spécifique pour cet événement.
     * @param e L'événement MouseEvent associé à la sortie de la souris.
     */
    @Override
    public void mouseExited(MouseEvent e) {
        // Cette méthode est vide car elle n'a pas de fonctionnalité spécifique pour cet événement.
    }

    /**
     * Gère l'événement de déplacement de la souris lorsqu'un bouton est enfoncé.
     * Si l'outil sélectionné est le mode "Libre", dessine une ligne en suivant les mouvements de la souris.
     * Sinon, dessine un élastique pour représenter la forme en cours de création.
     * @param e L'événement MouseEvent associé au déplacement de la souris.
     */
    @Override
    public void mouseDragged(MouseEvent e) {    	
        // Récupérer l'outil sélectionné à partir du contrôleur associé au panneau boutons
        String boutonSelectionne = this.formeChoisie;
        boolean statePlein = this.monPanneau.getFenetre().getPanneauBoutons().getStatePlein();
        
        // Effacer le dernier élastique dessiné
        this.dessinerElastique(this.x1, this.y1, this.x2, this.y2);
        
        // Créer la forme correspondante à la sélection
        Forme nouvelleForme = null;
        int xInf = Math.min(this.x1, this.x2);
        int yInf = Math.min(this.y1, this.y2);
        int largeur = Math.abs(this.x2 - this.x1);
        int hauteur = Math.abs(this.y2 - this.y1);
        
        // Si l'outil sélectionné est "Libre", dessiner une ligne en suivant les mouvements de la souris
        if (boutonSelectionne.equals("Libre") || boutonSelectionne.equals("Gomme")) {
        	if (this.isLeftMouseButtonPressed) {
        		this.dessin.addPoint(e.getPoint(), this.monPanneau);
        	}
        } 
        // Sinon, dessiner un élastique pour représenter la forme en cours de création
        else {
        	this.x2 = e.getX();
        	this.y2 = e.getY();
            dessinerElastique(this.x1, this.y1, this.x2, this.y2);
        }
        
        // RECTANGLE
        if (boutonSelectionne.equals("Rectangle")) {
            nouvelleForme = new Rectangle(new Point(xInf, yInf), largeur, hauteur, statePlein, 
                    this.monPanneau.getFenetre().getPanneauBoutons().getControleurCouleur().getCouleurChoisie(),
                    this.monPanneau.getFenetre().getPanneauBoutons().getLargeur());
        } 
        // CARRE
        else if (boutonSelectionne.equals("Carré")) {
            int cote = Math.max(largeur, hauteur);
            nouvelleForme = new Carre(new Point(xInf, yInf), cote, statePlein, 
            		this.monPanneau.getFenetre().getPanneauBoutons().getControleurCouleur().getCouleurChoisie(),
                    this.monPanneau.getFenetre().getPanneauBoutons().getLargeur());
        } 
        // CERCLE
        else if (boutonSelectionne.equals("Cercle")) {
            int cote = Math.max(largeur, hauteur);
            nouvelleForme = new Cercle(new Point(xInf, yInf), cote, statePlein, 
            		this.monPanneau.getFenetre().getPanneauBoutons().getControleurCouleur().getCouleurChoisie(),
                    this.monPanneau.getFenetre().getPanneauBoutons().getLargeur());
        } 
        // ELLIPSE
        else if (boutonSelectionne.equals("Ellipse")) {
            nouvelleForme = new Ellipse(new Point(xInf, yInf), largeur, hauteur, statePlein, 
            		this.monPanneau.getFenetre().getPanneauBoutons().getControleurCouleur().getCouleurChoisie(),
                    this.monPanneau.getFenetre().getPanneauBoutons().getLargeur());
        } 
        // LIGNE
        else if (boutonSelectionne.equals("Ligne")) {
            this.pointFin = e.getPoint();
            nouvelleForme = new Ligne(this.pointDepart, this.pointFin, 
            		this.monPanneau.getFenetre().getPanneauBoutons().getControleurCouleur().getCouleurChoisie(),
                    this.monPanneau.getFenetre().getPanneauBoutons().getLargeur());
        }
        
        // Vérifier si une nouvelle forme a été créée
        if (nouvelleForme != null || this.etatDessin == true) {
            if (nouvelleForme != null) {
            	// Ajouter la forme au panneau dessin
            	this.monPanneau.ajouterPrevisu(nouvelleForme);
            }
            else if (this.etatDessin == true) {
            	this.monPanneau.ajouterPrevisu(nouvelleForme);
            }
        }
        
        // Demander au panneau de dessin de se redessiner pour afficher la nouvelle forme
        this.monPanneau.repaint();
    }

    /**
     * Gère l'événement de déplacement de la souris.
     * Cette méthode est vide car elle n'a pas de fonctionnalité spécifique pour cet événement.
     * @param e L'événement MouseEvent associé au déplacement de la souris.
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        // Cette méthode est vide car elle n'a pas de fonctionnalité spécifique pour cet événement.
    }
}