package com.projetdev.controleur;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JColorChooser;

import com.projetdev.vue.Fenetre;

/**
 * La classe GestionCouleur est une classe qui gère la couleur utilisée dans l'application.
 * Elle implémente ActionListener.
 */
public class GestionCouleur implements ActionListener {
	/**
	 * Couleur par défaut utilisée par l'application.
	 */
	private Color couleurParDefaut;

	/**
	 * Couleur choisie par l'utilisateur dans l'application.
	 */
	private Color couleurChoisie;
	
	/**
	 * Instance de la fenêtre principale.
	 */
	private Fenetre maFenetre;
    
    /**
     * Constructeur de la gestion de couleur.
     * @param couleurParDefaut La couleur par défaut.
     * @param maFenetre Instance de la Fenêtre principale.
     */
    public GestionCouleur(Color couleurParDefaut, Fenetre maFenetre) {
    	this.couleurParDefaut = couleurParDefaut;
    	this.maFenetre = maFenetre;
    }
    
    /**
     * Méthode pour obtenir la couleur choisie par l'utilisateur.
     * @return La couleur choisie par l'utilisateur, ou noir si aucune couleur n'a été choisie.
     */
    public Color getCouleurChoisie() {
    	if(this.couleurChoisie == null) {
    		return Color.BLACK;
    	}
    	else {
    		return this.couleurChoisie;
    	}
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		// Ouvrir le sélecteur de couleur avec la couleur par défaut et obtenir la couleur choisie par l'utilisateur
		Color c = JColorChooser.showDialog(this.maFenetre, "Choisir la couleur", couleurParDefaut);
		if (c != null) {
			couleurChoisie = c; // Mettre à jour la couleur choisie
			this.maFenetre.getPanneauDessin().setCouleurActuelle(this.couleurChoisie);
		}
	}
}
