# Package com.projetdev.controleur

Ce package contient les classes qui contrôlent les interactions utilisateur dans l'application.

## Contenu

1. [GestionBoutons.java](GestionBoutons.java): La classe qui gère les boutons de contrôle pour les formes graphiques.
1. [GestionCouleur.java](GestionCouleur.java): La classe qui gère la couleur utilisée dans l'application.
1. [GestionMenu.java](GestionMenu.java): La classe qui gère les actions liées aux éléments de menu.
1. [GestionSouris.java](GestionSouris.java): La classe qui gère les interactions de la souris pour le dessin des formes.

Chaque classe offre des fonctionnalités spécifiques pour le contrôle et la gestion des éléments de l'interface utilisateur de l'application.