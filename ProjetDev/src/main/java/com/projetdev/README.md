# Package com.projetdev

Ce package contient le fichier principal de l'application.

## Contenu

1. [LogicielDeDessins.java](LogicielDeDessins.java): Le fichier principal de l'application qui initialise et lance l'interface utilisateur.

Ce fichier est responsable de l'initialisation de l'interface utilisateur de l'application de dessin.

## Auteur

Ce fichier a été écrit par [Armand Girard].