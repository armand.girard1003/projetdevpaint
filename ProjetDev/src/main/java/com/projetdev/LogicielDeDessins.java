package com.projetdev;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.SwingUtilities;

import com.projetdev.vue.*;
import com.projetdev.controleur.*;

/**
 * La classe Application est la classe principale de l'application qui initialise et lance l'interface utilisateur.
 */
public class LogicielDeDessins {

	/**
     * Constructeur par défaut de la classe Application.
     * Ce constructeur crée une instance de la classe Application.
     */
    public LogicielDeDessins() {}
	
    /**
     * Méthode principale de l'application.
     * @param args Les arguments de ligne de commande (non utilisés dans cette application).
     */
    public static void main(String[] args) {
    	SwingUtilities.invokeLater(()-> {
        	// Créer une instance de Fenetre avec le nom et la dimension spécifiés
        	Fenetre maFenetre = new Fenetre("PAINT", new Dimension(1000, 800));
        	
            // Initialiser le contrôleur de souris
            GestionSouris controleurSouris = new GestionSouris();
            
            // Initialiser le panneau de dessin avec une dimension et une couleur de fond spécifiées
            PanneauDessin panneauDessin = new PanneauDessin(maFenetre, Color.white);

            // Initialiser le panneau de boutons
            PanneauBoutons panneauBoutons = new PanneauBoutons(maFenetre);

            // Associer le panneau de dessin au contrôleur de souris
            controleurSouris.setPanneauDessin(panneauDessin);

            // Ajouter des écouteurs de souris au panneau de dessin
            panneauDessin.addMouseListener(controleurSouris);
            panneauDessin.addMouseMotionListener(controleurSouris);

            // Configurer la fenêtre avec les panneaux de boutons et de dessin
            maFenetre.setPanneauBoutons(panneauBoutons);
            maFenetre.setPanneauDessin(panneauDessin);

            // Ajuster la taille de la fenêtre aux composants et la rendre visible
            maFenetre.pack();
            maFenetre.setVisible(true);
    	});
    }
}
