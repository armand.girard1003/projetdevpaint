# Projet de Logiciel de Dessin

Ce projet est une application de dessin permettant de créer et de manipuler des formes graphiques.

## Structure du Projet

Le projet est organisé en plusieurs packages et ressources :

- **Package `com.projetdev`**: Contient les fichiers principaux de l'application.
  - **`LogicielDeDessins.java`**: La classe principale de l'application qui initialise et lance l'interface utilisateur.
- **Package `com.projetdev.controleur`**: Contient les classes qui gèrent les interactions utilisateur.
  - **`GestionBoutons.java`**: Gère les boutons de contrôle pour les formes graphiques.
  - **`GestionCouleur.java`**: Gère la couleur utilisée dans l'application.
  - **`GestionMenu.java`**: Gère les actions liées aux éléments de menu.
  - **`GestionSouris.java`**: Gère les interactions de la souris pour le dessin des formes.
- **Package `com.projetdev.vue`**: Contient les classes qui représentent la vue de l'application.
  - **`Fenetre.java`**: Gère la fenêtre principale de l'application.
  - **`PanneauBoutons.java`**: Contient le panneau de boutons d'interaction de l'application.
  - **`PanneauDessin.java`**: Représente le panneau de dessin pour afficher des formes graphiques.
- **Dossier `resources`**: Contient les ressources utilisées par l'application.
  - **Dossier `img`**: Contient les images utilisées pour enrichir l'interface utilisateur de l'application.

## Contenu du Dossier "resources"

Le dossier "resources" contient plusieurs ressources utilisées par l'application, notamment :

- **Images** : Utilisées pour les icônes et les logos de l'application.
  - `carre.png` : Icône de carré.
  - `cercle.png` : Icône de cercle.
  - `ellipse.png` : Icône d'ellipse.
  - `libre.png` : Icône de dessin libre.
  - `ligne.png` : Icône de ligne.
  - `logo.jpg` : Logo en fond de panneau de dessins.
  - `logo.png` : Logo de la fenêtre.
  - `rectangle.png` : Icône de rectangle.

Ces ressources sont utilisées pour améliorer l'esthétique et la convivialité de l'interface utilisateur de l'application de dessin.
